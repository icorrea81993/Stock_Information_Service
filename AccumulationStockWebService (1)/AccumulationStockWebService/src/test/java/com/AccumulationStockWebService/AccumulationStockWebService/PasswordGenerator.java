package com.AccumulationStockWebService.AccumulationStockWebService;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class PasswordGenerator {
    public static void main(String[] args){
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String rawPasswordOne = "Accumulation";
        String encodedPasswordOne = encoder.encode(rawPasswordOne);
        System.out.println(encodedPasswordOne);
        String rawPasswordTwo = "Admin";
        String encodedPasswordTwo = encoder.encode(rawPasswordTwo);
        System.out.println(encodedPasswordTwo);
    }
}
