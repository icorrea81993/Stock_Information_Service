package com.AccumulationStockWebService.AccumulationStockWebService.Repository;

import com.AccumulationStockWebService.AccumulationStockWebService.Model.StockInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StockInformationRepository extends JpaRepository<StockInformation, Integer> {
}
