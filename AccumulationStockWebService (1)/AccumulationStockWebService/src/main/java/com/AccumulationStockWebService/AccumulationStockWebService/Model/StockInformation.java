package com.AccumulationStockWebService.AccumulationStockWebService.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigInteger;
import java.util.Objects;

@Entity
public class StockInformation {

    @Id
    @GeneratedValue
    public int id;

    public String assetName;
    public String assetTicker;
    public String sector;
    public float priceToEarningsRatio;
    public float forwardPriceToEarningsRatio;
    public float priceToBookRatio;
    public float priceEarningsToGrowthRatio;

    public StockInformation(int id, String assetName, String assetTicker, String sector, float priceToEarningsRatio, float forwardPriceToEarningsRatio, float priceToBookRatio, float priceEarningsToGrowthRatio) {
        this.id = id;
        this.assetName = assetName;
        this.assetTicker = assetTicker;
        this.sector = sector;
        this.priceToEarningsRatio = priceToEarningsRatio;
        this.forwardPriceToEarningsRatio = forwardPriceToEarningsRatio;
        this.priceToBookRatio = priceToBookRatio;
        this.priceEarningsToGrowthRatio = priceEarningsToGrowthRatio;
    }

    public StockInformation() {

    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getAssetTicker() {
        return assetTicker;
    }

    public void setAssetTicker(String assetTicker) {
        this.assetTicker = assetTicker;
    }

    public float getPriceToEarningsRatio() {
        return priceToEarningsRatio;
    }

    public void setPriceToEarningsRatio(float priceToEarningsRatio) {
        this.priceToEarningsRatio = priceToEarningsRatio;
    }

    public float getForwardPriceToEarningsRatio() {
        return forwardPriceToEarningsRatio;
    }

    public void setForwardPriceToEarningsRatio(float forwardPriceToEarningsRatio) {
        this.forwardPriceToEarningsRatio = forwardPriceToEarningsRatio;
    }

    public float getPriceToBookRatio() {
        return priceToBookRatio;
    }

    public void setPriceToBookRatio(float priceToBookRatio) {
        this.priceToBookRatio = priceToBookRatio;
    }

    public float getPriceEarningsToGrowthRatio() {
        return priceEarningsToGrowthRatio;
    }

    public void setPriceEarningsToGrowthRatio(float priceEarningsToGrowthRatio) {
        this.priceEarningsToGrowthRatio = priceEarningsToGrowthRatio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StockInformation)) return false;
        StockInformation that = (StockInformation) o;
        return getId() == that.getId() &&
                Float.compare(that.getPriceToEarningsRatio(), getPriceToEarningsRatio()) == 0 &&
                Float.compare(that.getForwardPriceToEarningsRatio(), getForwardPriceToEarningsRatio()) == 0 &&
                Float.compare(that.getPriceToBookRatio(), getPriceToBookRatio()) == 0 &&
                Float.compare(that.getPriceEarningsToGrowthRatio(), getPriceEarningsToGrowthRatio()) == 0 &&
                Objects.equals(getAssetName(), that.getAssetName()) &&
                Objects.equals(getAssetTicker(), that.getAssetTicker()) &&
                Objects.equals(getSector(), that.getSector());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getAssetName(), getAssetTicker(), getSector(), getPriceToEarningsRatio(), getForwardPriceToEarningsRatio(), getPriceToBookRatio(), getPriceEarningsToGrowthRatio());
    }

    @Override
    public String toString() {
        return "StockInformation{" +
                "id=" + id +
                ", assetName='" + assetName + '\'' +
                ", assetTicker='" + assetTicker + '\'' +
                ", sector='" + sector + '\'' +
                ", priceToEarningsRatio=" + priceToEarningsRatio +
                ", forwardPriceToEarningsRatio=" + forwardPriceToEarningsRatio +
                ", priceToBookRatio=" + priceToBookRatio +
                ", priceEarningsToGrowthRatio=" + priceEarningsToGrowthRatio +
                '}';
    }
}
