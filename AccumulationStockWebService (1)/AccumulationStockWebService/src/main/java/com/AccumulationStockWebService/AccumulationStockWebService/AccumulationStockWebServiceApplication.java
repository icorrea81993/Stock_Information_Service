package com.AccumulationStockWebService.AccumulationStockWebService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.websocket.Session;

@SpringBootApplication
public class AccumulationStockWebServiceApplication {

	public static void main(String[] args) {

		SpringApplication.run(AccumulationStockWebServiceApplication.class, args);
	}

}
