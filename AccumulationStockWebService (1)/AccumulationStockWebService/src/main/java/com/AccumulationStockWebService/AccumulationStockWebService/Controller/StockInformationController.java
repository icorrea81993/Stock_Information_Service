package com.AccumulationStockWebService.AccumulationStockWebService.Controller;

import com.AccumulationStockWebService.AccumulationStockWebService.Model.StockInformation;
import com.AccumulationStockWebService.AccumulationStockWebService.Repository.StockInformationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class StockInformationController {

    @Autowired
    public StockInformationRepository stockInfoRepo;

    @GetMapping("/stockInformation")
    public List<StockInformation> getAllStockInfo() {
        return stockInfoRepo.findAll();
    }

    @GetMapping("/stockInformation/{id}")
    public Optional<StockInformation> retrieveAsset(@PathVariable int id){
        return stockInfoRepo.findById(id);
    }

    @DeleteMapping("/stockInformation/{id}")
    public void deleteAsset(@PathVariable int id) {
        stockInfoRepo.deleteById(id);
    }
}
